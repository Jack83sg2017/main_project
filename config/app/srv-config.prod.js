exports.config = {
    apiServerConfig: {
        generalAPIServerUrl: "https://www.goggle.com/"
    },
    restServiceServerConfig: {
        headers: {
            "Accept": "application/json"
        },
        timeout: {
            "timeout": 10,
        }
    },
    contentServiceConfig: {
        contentURI: "",
        lazyLoadingStartupModules: "",
        bundlesModules: "",
        moduleLoadOnStartup: ""
    },
    ruleServiceConfig: {
        ruleServiceURI: "",
        moduleLoadOnStartup: ""
    },
    loggingServiceConfig: {
        logLevel: "DEBUG",
        isConsoleLogEnabled: false,
        isServerPushEnabled: false,
        thresholdBuffer: 12
    },
    preloadingModuleConfig: {
        preloadModules: ""
    }
}