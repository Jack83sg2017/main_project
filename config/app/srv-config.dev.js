exports.config = {
    apiServerConfig: {
        generalAPIServerUrl: "https://www.goggle.com/"
    },
    restServiceServerConfig: {
        headers: {
            "Accept": "application/json"
        },
        timeout: {
            "timeout": 10,
        }
    },
    contentServiceConfig: {
        contentURI: "",
        lazyLoadingStartupModules: "",
        bundlesModules: "",
        moduleLoadOnStartup: ""
    },
    ruleServiceConfig: {
        ruleServiceURI: "",
        moduleLoadOnStartup: ""
    },
    loggingServiceConfig: {
        logLevel: "DEBUG",
        isConsoleLogEnabled: true,
        isServerPushEnabled: false,
        thresholdBuffer: 10
    },
    preloadingModuleConfig: {
        preloadModules: ""
    }
}