import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { APP_BASE_HREF } from '@angular/common';
import { AppService } from './app.service';
import { 
  ServicesLibraryModule
} from "services-library";

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ServicesLibraryModule.forRoot(),
  ],
  providers: [
    AppService,
    { provide: APP_BASE_HREF, useValue: "./" }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(
    appService: AppService
  ) {
    appService.setup();
  }
}
