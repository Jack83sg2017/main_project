import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { LandingComponent } from "./bootstrap/landing/pages/landing.component";

const appROUTES: Routes = [
  {
    path:"",
    redirectTo: "main",
    pathMatch: "full"
  },
  {
    path: "main",
    component: LandingComponent
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(
      appROUTES,
      {
        enableTracing: false
      }
    )
  ],
  exports: [RouterModule]
})

export class AppRoutingModule { }