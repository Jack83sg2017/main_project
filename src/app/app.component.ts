import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { AnimationService } from 'services-library';

@Component({
  selector: 'app',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  constructor(
    private router: Router,
    private animationService: AnimationService
  ){}

  ngOnInit(): void {
    document.documentElement.style.display = "block";
    document.documentElement.style.visibility = "visible";
    this.animationService.routingService("forward", this.router.url, "main", {});
  }
  title = 'main_project';
}
