import { Injectable } from "@angular/core";

import * as _ from 'lodash';
import { ServiceServiceConfig } from "services-library/lib/serviceServerConfig/serviceServerConfig.config.interface";
import { environment } from "src/environments/environment";
import {
    ServiceServerConfigService
} from "services-library"

@Injectable()
export class AppService {
    constructor(
        private serviceServerConfigService: ServiceServerConfigService,
    ) {}

    public setup(): void {
        this.initServiceConfig();
    }

    private initServiceConfig(): void {
        let serviceConfig: ServiceServiceConfig;

        if(environment.production) {
            serviceConfig = require("../../config/app/srv-config.prod").config;
        } else {
            serviceConfig = require("../../config/app/srv-config.dev").config;
        }

        this.serviceServerConfigService.setServiceServerConfig(serviceConfig);
    }
}
